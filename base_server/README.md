# Overview

This directory contains a simple C-based server that is meant to provide a minimal base for
prototypes of communication backends (such as dpdk or io\_uring) and for wire protocols (such as
protobuf).
