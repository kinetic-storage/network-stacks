#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netdb.h>

#define TEST_PORT         9925

#define MAX_RETRIES       3

#define NUM_SECONDS       1
#define NUM_MICRO_SECONDS 0

struct sockaddr* get_bindable_address(unit16_t port_as_int) {
    struct sockaddr_in *bind_addr = (struct sockaddr_in *) malloc (sizeof(struct sockaddr_in));

    bind_addr->sin_family      = AF_INET;
    bind_addr->sin_addr.s_addr = htonl(INADDR_ANY);
    bind_addr->sin_port        = htons(port_as_int);

    return (struct sockaddr *) bind_addr;
}

int socket_for_listening() {
    int socket_id = socket(AF_INET, SOCK_DGRAM, 0);
}

void listen_for_clients(int socket_id) {
    int num_attempts = 0;

    while (1) {
        if (select_call(socket_id, NUM_SECONDS, NUM_MICRO_SECONDS)) {
        }

        else if (num_attempts < MAX_RETRIES) {
            num_attempts++;
        }
    }
}

int main(int argc, char **argv) {
    int              socket_id = -1;
    int              port_id   = -1;
    struct sockaddr *bind_addr = get_bindable_address(TEST_PORT);

    /* prepare socket */
    socket_id = socket(AF_INET, SOCK_DGRAM, 0);

    if (socket_id == -1) {
        perror("unable to open socket");
        exit(EXIT_FAILURE);
    }

    /* bind address to socket */
    int bind_result = bindMod(socket_id, bind_addr, sizeof(struct sockaddr_in));

    if (bind_result < 0) {
        perror("unable to bind address");
        exit(EXIT_FAILURE);
    }

    /* listen for clients */
    listen_for_clients(socket_id);


    return EXIT_SUCCESS;
}

int main (int argc, char **argv) {
    extern int    make_socket (uint16_t port);
           int    sock;
           int    i;
           struct sockaddr_in clientname;
           size_t size;
           fd_set active_fd_set, read_fd_set;

    /* Create the socket and set it up to accept connections. */
    sock = make_socket (PORT);

    if (listen (sock, 1) < 0) {
        perror ("listen");
        exit (EXIT_FAILURE);
    }

    /* Initialize the set of active sockets. */
    FD_ZERO (&active_fd_set);
    FD_SET (sock, &active_fd_set);

    while (1) {
        /* Block until input arrives on one or more active sockets. */
        read_fd_set = active_fd_set;

        if (select (FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) {
            perror ("select");
            exit (EXIT_FAILURE);
        }

        /* Service all the sockets with input pending. */
        for (i = 0; i < FD_SETSIZE; ++i) {
            if (FD_ISSET (i, &read_fd_set)) {
                if (i == sock) {
                    /* Connection request on original socket. */

                    int new;
                    size = sizeof (clientname);
                    new = accept (sock,
                                  (struct sockaddr *) &clientname,
                                  &size);
                    if (new < 0)
                      {
                        perror ("accept");
                        exit (EXIT_FAILURE);
                      }
                    fprintf (stderr,
                             "Server: connect from host %s, port %hd.\n",
                             inet_ntoa (clientname.sin_addr),
                             ntohs (clientname.sin_port));
                    FD_SET (new, &active_fd_set);
                }

                else {
                    /* Data arriving on an already-connected socket. */
                    if (read_from_client (i) < 0) {
                        close (i);
                        FD_CLR (i, &active_fd_set);
                    }
                }
            }
        }
    }

}
