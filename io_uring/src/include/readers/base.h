#include <sys/types.h>
#include <sys/stat.h>

#include <unistd.h>
#include <fcntl.h>

enum data_delim {
    COMMA,
    TAB,
    PIPE,
};

/**
 * Functions that return an IO handle.
 */
int fd_for_file(char *path_to_file);

/**
 * Functions that data in a buffer.
 */
ssize_t read_handle_single(int handle_as_fd, size_t byte_count);
ssize_t read_handle_multi(int handle_as_fd,  size_t byte_count, int num_reads);
ssize_t read_handle_uring(int handle_as_fd,  size_t byte_count, int num_reads);

/**
 * Functions that interpret raw data into a structure
 */
struct tuple* tuples_from_data_as_dsv(data_delim delimiter, char *data_buffer);
