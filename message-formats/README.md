# Overview

# Kinetic Protobuf Definition

## Message

### Reserved

Tags `1`, `2`, and `3` are reserved and should not be used for any other fields.

### Primitives

commandBytes

    embedded message providing HMACauth request and all auth type responses

### AuthType

AuthType determines how the the message is to be processed:

    If unknown, close the connection
    For normal traffic, if HMAC of the command is correct, process the command.
	For device unlock and ISE command. PIN ops must come over the TLS connection; otherwise,
    close the connection.
	When the device needs to communicate with the user

Type of authorization used for a message.

#### HMAC Auth

HMACauth for Normal messages.

For normal requests/responses to/from the device.
Allowed once the device is unlocked, and HMAC provided for:
authenticity, integrity, and role enforcement.
    

#### PIN Auth

PINauth for pin-based operations (e.g. device unlock and device erase).

Pin based authentication for Pin operations.

## Command

### Header
message header

#### Reserved

2  is reserved.
8  is reserved, do not use
11 is reserved

#### Primitives

(clusterVersion)
"cluster" is the  number of the cluster definition. If this is incompatible,
the request is rejected. By default the value is 0, allowing systems not
using cluster versioning to ignore this field in the header and in the setup.

(connectionID)
A unique number for this connection between the source and target. The device can change this
number and the client must continue to use the new number and the number must remain
constant during the session.

(sequence)
the sequence of this request in this TCP connection. As long as this value is getting larger we have
strong ordering and replay prevention within a session. This combined with the time and connectionID
provides strong ordering between sessions.

(ackSequence)
co-related sequence

(messageType)
operation code - put/get/delete/GetLog, etc.

(timeout)
Request timeout (in ms). This is the amount of time that this request should take. If this timeout
is triggered, there are three possible results that can be returned.
- SERVICE_BUSY meaning that the request was still on the queue waiting to be executed
- EXPIRED meaning that a long running operation was stopped because the time expired.
- DATA_ERROR meaning that the request was in process, but that the error recovery was not
complete at the time that the time expired

(earlyExit)
If true, requests will not attempt multi revolution recoveries even if the timeout has not occurred.
In this case the result will be DATA_ERROR. To have the device exhaust all possible error recovery, leave
this field off or set to false, and make sure that the timeout is set to be longer than any possible queue
time and error recovery time. On a disk device, the maximum error recovery time could be seconds.
Once all possible data recovery operations are complete and have not succeeded, PERM_DATA_ERROR will be
returned.

(priority)
Priority is a simple integer that determines the priority of this
request. All activity at a higher priority will execute before that
of lower priority traffic. A higher number is higher priority.

(TimeQuanta)
A hint of how long a job should run before yielding. Specified in
miliseconds. A value of 0 indicates that the operation can perform one
sub operation and then check to see if there are other sub higher
priority operations. An example of a sub-operation might be a single put
in a P2P operation, etc.

(batchID)
batch id to be included in each command of a batch operation
this id is generated by client library and must be unique
within the same connection.


### Body
message body

#### Reserved

5 is reserved.


#### KeyValue

key or value op

#### Range

range operation

#### Setup

set up operation

#### Peer to Peer (P2P) Operation

Peer to Peer operations.

#### Get Log

GetLog

#### Security

set up security


#### Pin Operation

Perform Pin-based operations


#### Batch

batch operation
This is included in the END\_BATCH and END\_BATCH\_RESPONSE.

##### Primitives

(count)
set by the client library in END_BATCH request message.
the total number of operations in the batch

(sequence)
set by the drive in END_BATCH_RESPONSE message.
If a batch is committed successfully, all sequence Ids of those
commands (PUT/DELETE) performed in the batch are
added in the END_BATCH_RESPONSE message.

(failedSequence)
This field is set by the drive if a batch commit failed.
The sequence of the first operation to fail in the batch.
There is no guarantee that the previous sequences would have succeeded.


#### Power Management

power management


### Status
operation status
